package com.tercadaserra.loja.service;

import com.tercadaserra.loja.model.CarrinhoItem;
import com.tercadaserra.loja.model.Produto;

import java.util.ArrayList;
import java.util.List;

public class CarrinhoService {

    private List<CarrinhoItem> itens = new ArrayList<CarrinhoItem>();

    public List<CarrinhoItem> getItens() {
        return itens;
    }

    public void incluirProdutoNoCarrinho(Produto produto, Integer quantidade){
        CarrinhoItem carrinhoItem = new CarrinhoItem();
        carrinhoItem.setProduto(produto);
        carrinhoItem.setValorItem(produto.getValorProduto() * quantidade);
        carrinhoItem.setQtdItem(quantidade);
        itens.add(carrinhoItem);
    }

    public void exibirCarrinho(){
        itens.forEach(item -> {
            System.out.println("Nome Produto: " + item.getProduto().getNomeProduto() + " QTD: " + item.getQtdItem() + " Valor: " + item.getValorItem());
        });
    }

    public CarrinhoItem consultarItem(String n){
        for (CarrinhoItem c : itens){
            if (c.getProduto().getNomeProduto().equals(n)){
                return c;
            }
        }
        return null;
    }
    public void removerItem(CarrinhoItem carrinhoItem){
        for (CarrinhoItem c : itens){
            if (c.getProduto().getNomeProduto().equals(carrinhoItem.getProduto().getNomeProduto())){
                itens.remove(c);
            }
        }
    }

}




