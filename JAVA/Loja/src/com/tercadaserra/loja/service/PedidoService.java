package com.tercadaserra.loja.service;

import com.tercadaserra.loja.model.CarrinhoItem;
import com.tercadaserra.loja.model.Cliente;
import com.tercadaserra.loja.model.Pedido;
import com.tercadaserra.loja.model.PedidoItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PedidoService {

    ProdutoService produtoService = new ProdutoService();

    private List<Pedido> pItems = new ArrayList<>();
    public List<Pedido> getpItems() {
        return pItems;
    }


    public void incluirPedido(List<CarrinhoItem> listaDeItensDoCarrinho, Cliente cliente){

        Pedido pedido = new Pedido();
        pedido.setCliente(cliente);
        pedido.setnPedido(new Random().nextInt(1000000000));
        pedido.setDate(10);

        for (CarrinhoItem ci : listaDeItensDoCarrinho){
            System.out.println("Nome Produto: "
                    + ci.getProduto().getNomeProduto()
                    + " QTD: "
                    + ci.getQtdItem()
                    + " Valor: "
                    + ci.getValorItem());

            PedidoItem pedidoItem = new PedidoItem();
            pedidoItem.setProduto(ci.getProduto());
            pedidoItem.setQtdItem(ci.getQtdItem());

        }

        pItems.add(pedido);
    }

    public void exibirListPedido(){
        for(Pedido p : pItems){
            System.out.println("Numero Pedido: "
                    + p.getnPedido()
                    + "\nData de chegada: "
                    + p.getDate()
                    + " dias\n"
                    + p.getCliente().getcEndereco());
        }
    }

}