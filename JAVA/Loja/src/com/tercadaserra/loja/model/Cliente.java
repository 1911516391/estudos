package com.tercadaserra.loja.model;

public class Cliente {

    private String cNome;
    private String cCpf;
    private int cTelefone;
    private String cEndereco;
    private Pedido pedidos[];

    public void setcNome(String cNome) {
        this.cNome = cNome;
    }

    public void setcCpf(String cCpf) {
        this.cCpf = cCpf;
    }

    public void setcTelefone(int cTelefone) {
        this.cTelefone = cTelefone;
    }

    public void setcEndereco(String cEndereco) {
        this.cEndereco = cEndereco;
    }

    public String getcNome() {
        return cNome;
    }

    public String getcCpf() {
        return cCpf;
    }

    public int getcTelefone() {
        return cTelefone;
    }

    public String getcEndereco() {
        return cEndereco;
    }
}

