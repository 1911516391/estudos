package com.tercadaserra.loja.model;

public class PedidoItem {

    private Produto produto;
    private Pedido pedido;
    private int qtdItem;

    public Produto getProduto() {
        return produto;
    }
    public Pedido getPedido() {
        return pedido;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }

    public int getQtdItem() {
        return qtdItem;
    }

    public void setQtdItem(int qtdItem) {
        this.qtdItem = qtdItem;
    }
}
