package com.tercadaserra.loja.model;

public class Imagem {

    private int imgAltura;
    private int imgLargura;
    private String url;

    public Imagem(int imgAltura, int imgLargura, String url){
        this.imgAltura = imgAltura;
        this.imgLargura = imgLargura;
        this.url = url;

    }

    public int getImgAltura() {
        return imgAltura;
    }

    public void setImgAltura(int imgAltura) {
        this.imgAltura = imgAltura;
    }

    public int getImgLargura() {
        return imgLargura;
    }

    public void setImgLargura(int imgLargura) {
        this.imgLargura = imgLargura;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
