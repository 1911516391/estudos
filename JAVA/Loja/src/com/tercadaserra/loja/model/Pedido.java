package com.tercadaserra.loja.model;

import java.util.Date;

public class Pedido {

    private Cliente cliente;
    private int nPedido;
    private int date;
    private PedidoItem pedidoItem[];

    public int getnPedido() {
        return nPedido;
    }

    public void setnPedido(int nPedido) {
        this.nPedido = nPedido;
    }

    public PedidoItem[] getPedidoItem() {
        return pedidoItem;
    }

    public void setPedidoItem(PedidoItem[] pedidoItem) {
        this.pedidoItem = pedidoItem;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
}
